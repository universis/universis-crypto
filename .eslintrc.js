module.exports = {
  env: {
    "browser": false,
    "node": true,
    "es6": true,
    "jquery": false
  },
  globals: {
    "describe": false,
    "IntlPolyfill": false,
    "before": false,
    "after": false,
    "it": false
  },
  extends: [
    "eslint:recommended",
    'prettier'
  ],
  rules: {
    "no-console": "error",
    "no-invalid-this": "warn",
    "no-undef": "error",
    "no-unused-vars": "warn",
    "no-var": [
      "error"
    ],
    "strict": [
      2,
      "never"
    ]
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module"
  }
}


